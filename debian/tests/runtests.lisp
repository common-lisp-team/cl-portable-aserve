(require "asdf")

;; CL-IRONCLAD is broken on i386 with SBCL.
;; If it were arch:any, we would remove it on that arch.
(when (and (member :sbcl *features*)
	   (member (or (uiop:getenv "DEB_HOST_ARCH")
		       (uiop:stripln
			(uiop:run-program '("dpkg" "--print-architecture")
					  :output :string)))
		   '("i386") :test #'string=))
  (uiop:quit 0))

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "acl-compat")
  (asdf:load-system "htmlgen")
  (asdf:load-system "aserve")
  (asdf:load-system "webactions"))
